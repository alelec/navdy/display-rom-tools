#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd "${DIR}"

OS=$(uname)
ARCH=$(uname -m)

SUDO="sudo -E "

if [ "$OS" == "Linux" ]; then
	chmod +x "${DIR}"/tools/linux/*
	if [ "$ARCH" == "i386" ] || [ "$ARCH" == "i486" ] || [ "$ARCH" == "i586" ] || [ "$ARCH" == "amd64" ] || [ "$ARCH" == "x86_64" ] || [ "$ARCH" == "i686" ]; then # Linux on Intel x86/x86_64 CPU
		FASTBOOT="${DIR}"/tools/linux/fastboot
	elif [ "$ARCH" == "arm" ] || [ "$ARCH" == "armv6l" ] || [ "$ARCH" == "armv7l" ]; then # Linux on ARM CPU
		FASTBOOT="${DIR}"/tools/nexus-tools-673c086/bin/linux-arm-fastboot
	else
		echo "[WARN] Don't know what your linux platform is, will try i386."
		echo "[WARN] If fastboot fails find a copy that is suitable for you system and ensure it's in the path"
		FASTBOOT="${DIR}"/tools/linux/fastboot
	fi

elif [ "$OS" == "Darwin" ]; then
	chmod +x "${DIR}"/tools/macos/*
	FASTBOOT="${DIR}"/tools/macos/fastboot
    SUDO=""
else
	echo "[EROR] Don't have fastboot for your platform."
	echo "[EROR] OS: $OS"
	echo "[EROR] ARCH: $ARCH"
	echo "[EROR] find a copy that is suitable for you system and ensure it's in the path"
	FASTBOOT="fastboot"
fi

chmod +x $FASTBOOT

flash_failed () {
  echo "Error"; read -p "Press [Enter] to close"; exit $1; # exit for none-zero return code
}

export ANDROID_PRODUCT_OUT=.

$SUDO$FASTBOOT -w flashall
[ $? -eq 0 ] || flash_failed $?

echo "done"
popd
