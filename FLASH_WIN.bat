@echo off
pushd %~dp0

set PATH=%CD%\tools\windows;%PATH%
set ANDROID_PRODUCT_OUT=.

fastboot.exe -i 0x9886 -w flashall
IF %ERRORLEVEL% NEQ 0 goto error

pause
popd
exit /b

:error
pause
Echo flashing error
