#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd "${DIR}"

OS=$(uname)
ARCH=$(uname -m)

SUDO="sudo "

if [ "$OS" == "Linux" ]; then
	if [ "$ARCH" == "i386" ] || [ "$ARCH" == "i486" ] || [ "$ARCH" == "i586" ] || [ "$ARCH" == "amd64" ] || [ "$ARCH" == "x86_64" ] || [ "$ARCH" == "i686" ]; then # Linux on Intel x86/x86_64 CPU
		ADB="${DIR}"/linux/adb
	elif [ "$ARCH" == "arm" ] || [ "$ARCH" == "armv6l" ] || [ "$ARCH" == "armv7l" ]; then # Linux on ARM CPU
		ADB="${DIR}"/nexus-tools-673c086/bin/linux-arm-fastboot
	else
		echo "[WARN] Don't know what your linux platform is, will try i386."
		echo "[WARN] If fastboot fails find a copy that is suitable for you system and ensure it's in the path"
		ADB="${DIR}"/linux/fastboot
	fi

elif [ "$OS" == "Darwin" ]; then
	ADB="${DIR}"/macos/fastboot
        SUDO=""
else
	echo "[EROR] Don't have fastboot for your platform."
	echo "[EROR] OS: $OS"
	echo "[EROR] ARCH: $ARCH"
	echo "[EROR] find a copy that is suitable for you system and ensure it's in the path"
	ADB="fastboot"
fi

chmod +x $ADB

$SUDO$ADB logcat > logcat.txt

popd
