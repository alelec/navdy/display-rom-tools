@echo off
pushd %~dp0

echo -----------------------------------------------------------------------------------
echo After running this the display should switch into flash drive mode; the power light
echo should switch to green and the drive should pop up on your computer.
echo Create a folder called "update" on the flash drive if it doesn't already exist,
echo then copy the firmware pack zip file into the "update" folder.
echo Don't copy the extracted folder, just the downloaded zip.
echo Then eject the drive before taking the display to your car and start it up.
echo -----------------------------------------------------------------------------------
echo. 

set PATH=%CD%\windows;%PATH%
set ANDROID_PRODUCT_OUT=.

fastboot.exe -i 0x9886 flash recovery ..\recovery.img
IF %ERRORLEVEL% NEQ 0 goto error

fastboot.exe -i 0x9886 flash boot ..\recovery.img
IF %ERRORLEVEL% NEQ 0 goto error

fastboot.exe -i 0x9886 reboot
IF %ERRORLEVEL% NEQ 0 goto error

pause
popd
exit /b

:error
pause
Echo flashing error
